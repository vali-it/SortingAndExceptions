package sortingdemo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Car {

	private String manufacturer;
	private String model;
	private Date releaseDate;
	private int seatCount;

	public String getManufacturer() {
		return manufacturer;
	}

	public String getModel() {
		return model;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public int getSeatCount() {
		return seatCount;
	}

	public Car(String manufacturer, String model, String releaseDateStr, int seatCount) throws ParseException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		this.releaseDate = dateFormat.parse(releaseDateStr);
		
		this.manufacturer = manufacturer;
		this.model = model;
		this.seatCount = seatCount;
	}
	
	@Override
	public String toString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		
		return String.format("<%s, %s, %s, %s>", this.manufacturer, this.model, this.seatCount, dateFormat.format(this.releaseDate));
	}
}
