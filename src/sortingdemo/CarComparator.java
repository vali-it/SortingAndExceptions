package sortingdemo;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		if (o1.getManufacturer().compareTo(o2.getManufacturer()) == 0) {
			if (o1.getModel().compareTo(o2.getModel()) == 0) {
				if (o1.getSeatCount() == o2.getSeatCount()) {
					return o1.getReleaseDate().compareTo(o2.getReleaseDate());
				} else {
					return o1.getSeatCount() - o2.getSeatCount();
				}
			} else {
				return o1.getModel().compareTo(o2.getModel());
			}
		} else {
			return o1.getManufacturer().compareTo(o2.getManufacturer());
		}
		
	}

}
