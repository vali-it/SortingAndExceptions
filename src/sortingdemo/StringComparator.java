package sortingdemo;

import java.util.Comparator;

public class StringComparator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		// if o1 < o2 --> negatiivne arv
		// if o1 == o2 --> 0
		// if o1 > o2 --> positiivne arv
		return o1.compareTo(o2);
	}

	
}
