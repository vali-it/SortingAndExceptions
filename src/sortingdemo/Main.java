package sortingdemo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		String filePath = args.length > 0 ? args[0] : null;

		Scanner s = null;
		try {
			s = new Scanner(System.in);
			List<String> fileLines;
			do {
				fileLines = readFromFile(filePath);
				if (fileLines == null) {
					System.out.println("Palun sisesta faili asukoht:");
					filePath = s.nextLine();
				}
			} while (fileLines == null);
		} finally {
			s.close();
			System.out.println("Panin Scanneri ilusti kinni.");
		}

		System.out.println("Faili lugemine õnnestus!");

		// List<Car> cars = new ArrayList<>();
		// cars.add(new Car("Audi", "A4", "2000-12-11", 4));
		// cars.add(new Car("Ferrari", "F355", "1998-07-12", 2));
		// cars.add(new Car("Ferrari", "F355", "1998-01-03", 10));
		// cars.add(new Car("Tesla", "M3", "2016-12-05", 2));
		// cars.add(new Car("Ferrari", "F355", "1999-06-12", 5));
		// cars.add(new Car("Ferrari", "F355", "1999-06-12", 2));
		// cars.add(new Car("Audi", "A6", "2009-07-28", 5));
		// cars.add(new Car("Audi", "A6", "2006-12-05", 5));
		// System.out.println(cars);
		// cars.sort(new CarComparator());
		// System.out.println(cars);
		//
		// List<String> words = new ArrayList<>();
		// words.add("Number 2");
		// words.add("Number 3");
		// words.add("Number 1");
		// words.add("Number 9");
		// words.add("Number 7");
		// words.add("Number 5");
		// words.add("Number 1");
		// System.out.println(words);
		// // words.sort(new StringComparator());
		// // words.sort(new Comparator<String>() {
		// //
		// // @Override
		// // public int compare(String o1, String o2) {
		// // // if o1 < o2 --> negatiivne arv
		// // // if o1 == o2 --> 0
		// // // if o1 > o2 --> positiivne arv
		// // return o1.compareTo(o2);
		// // }
		// // });
		// words.sort((str1, str2) -> {
		// return str1.compareTo(str2);
		// });
		//
		// System.out.println(words);

	}

	private static List<String> readFromFile(String pathStr) {
		try {
			List<String> fileLines = Files.readAllLines(Paths.get(pathStr));
			return fileLines;
		} catch (IOException|NullPointerException e) {
			System.out.println("Appi, faili lugemine ebaõnnestus!");
			return null;
		}
	}

}
